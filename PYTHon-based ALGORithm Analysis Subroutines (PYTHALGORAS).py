
# coding: utf-8

# In[ ]:

get_ipython().magic(u'pylab inline')

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!! Core    : getDensity,pushParticles,getFields,
#!!!           getTotalDensity
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def getDensity(dArray,pArray,gArray):
    """Gather the charge density and current density of particles in the simulation. 
    The input variables are expected to be of the type, python dict:
        pArray['nParticles','nDimensions','charge','mass','x','vx'
               (,'y','vy','z','vz')]
    The output variables are expected to be of the type, python dict, will be modified and changed:
        fArray[*'charge or xCurrent',**'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
            *(either 'charge' or 'current')
            **(where nLengthTotal=nxLength*nyLength*nzLength)
        """
    
    ### Make local copies for simpler use
    nDim=pArray['nDimensions'];
    nPart=pArray['nParticles'];
    pCharge=pArray['charge'];
    pMass=pArray['mass'];
    dX=dArray['dx'];
    nxLength=dArray['nxLength'];
    cc=gArray['cc'];
    
    ### Set up integer boundary
    iBoundedLeft=[];
    iBoundedRight=[];
    for i in xrange(nxLength):
        iBoundedLeft.append(int(i-1));
        iBoundedRight.append(int(i+1));
        if(iBoundedRight[i]>=nxLength-1):
            iBoundedRight[i]=iBoundedRight[i]-nxLength+1
        if(iBoundedLeft[i]>=0):
            iBoundedLeft[i]=iBoundedLeft[i]+nxLength-1
    
    if(nDim>1):
        dY=dArray['dy'];
    if(nDim>2):
        dZ=dArray['dz'];    
    
    ### Initialize and zero out the charge and current densities
    dArray['charge'][:]=0.0;
    dArray['xCurrent'][:]=0.0;
    dArray['yCurrent'][:]=0.0;
    dArray['zCurrent'][:]=0.0;
    
    ### Gather the charge and currents
    for ip in xrange(nPart):
        iPart  = (((pArray['x'][ip]-gArray['xMin'])                  %(gArray['xMax']-gArray['xMin']))+gArray['xMin'])/dX;
        iLeft  = int(floor(iPart));
        iRight = iBoundedRight[int(iLeft+1)];
        
        distRight = (iPart - real(iLeft));
        distLeft  = (1.0 - distRight);  
        distRight = distRight*pArray['df'][ip];
        distLeft  = distLeft*pArray['df'][ip];  
        
        dArray['charge'][int(iLeft)]  = dArray['charge'][int(iLeft)] + (pCharge*distLeft);
        dArray['charge'][int(iRight)] = dArray['charge'][int(iRight)] + (pCharge*distRight);
        
        iPart  = (((pArray['x'][ip]-(0.5*pArray['vx'][ip])-gArray['xMin'])                  %(gArray['xMax']-gArray['xMin']))+gArray['xMin'])/dX;
        iLeft  = int(floor(iPart));
        iRight = iBoundedRight[int(iLeft+1)];
        
        distRight = (iPart - real(iLeft));
        distLeft  = (1.0 - distRight);  
        distRight = distRight*pArray['df'][ip];
        distLeft  = distLeft*pArray['df'][ip];
        
        dArray['yCurrent'][int(iLeft)]  = dArray['yCurrent'][int(iLeft)] + (cc*pCharge*pArray['vy'][ip]*distLeft);
        dArray['yCurrent'][int(iRight)] = dArray['yCurrent'][int(iRight)] + (cc*pCharge*pArray['vy'][ip]*distRight);
        
        dArray['zCurrent'][int(iLeft)]  = dArray['zCurrent'][int(iLeft)] + (cc*pCharge*pArray['vz'][ip]*distLeft);
        dArray['zCurrent'][int(iRight)] = dArray['zCurrent'][int(iRight)] + (cc*pCharge*pArray['vz'][ip]*distRight);
    
    ### Boundary conditions
    varsOfInterest=['charge','xCurrent','yCurrent','zCurrent'];
    for iv in xrange(len(varsOfInterest)):
        dArray[varsOfInterest[iv]][nxLength-1]=dArray[varsOfInterest[iv]][0]
    return

def getTotalDensity(dArray,dParticleArray):
    """Combine all density arrays into one."""
    numSpecies=len(dParticleArray);
    dArray['charge'][:]=0.0;
    dArray['xCurrent'][:]=0.0;
    dArray['yCurrent'][:]=0.0;
    dArray['zCurrent'][:]=0.0;
    for iSpecies in xrange(numSpecies):
        dArray['charge'][:]   = dArray['charge'][:]   + dParticleArray[iSpecies]['charge'][:];
        dArray['xCurrent'][:] = dArray['xCurrent'][:] + dParticleArray[iSpecies]['xCurrent'][:];
        dArray['yCurrent'][:] = dArray['yCurrent'][:] + dParticleArray[iSpecies]['yCurrent'][:];
        dArray['zCurrent'][:] = dArray['zCurrent'][:] + dParticleArray[iSpecies]['zCurrent'][:];
    return

def pushParticles(pArray,fArray,gArray):
    """Gather the charge density and current density of particles in the simulation. 
    The input variables are expected to be of the type, python dict, will be modified and changed:
        fArray[*'electricFieldX',('electricFieldY','electricFieldZ',)
                'magneticFieldX',('magneticFieldY','magneticFieldZ',)
               **'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
            *(either 'electric' or 'magnetic')
            **(where nLengthTotal=nxLength*nyLength*nzLength)
    The output variables are expected to be of the type, python dict, will be modified and changed:
        pArray['nParticles','nDimensions','charge','mass','x','vx'
               (,'y','vy','z','vz')]
    """
    ### Make local copies for simpler use
    nDim=pArray['nDimensions'];
    nPart=pArray['nParticles'];
    pCharge=pArray['charge'];
    pMass=pArray['mass'];
    dX=fArray['dx'];
    nxLength=fArray['nxLength'];
    
    ### Set up integer boundary
    iBoundedLeft=[];
    iBoundedRight=[];
    for i in xrange(nxLength):
        iBoundedLeft.append(int(i-1));
        iBoundedRight.append(int(i+1));
    iBoundedRight[nxLength-1]=0
    iBoundedLeft[0]=nxLength-1
    
    ### Initialize some temp field arrays
    ep=array(zeros([3]));
    bp=array(zeros([3]));
    up=array(zeros([3]));
    um=array(zeros([3]));
    bt=array(zeros([3]));
    vpro=array(zeros([3]));
    vproa=array(zeros([3]));
    
    const0=0.5*pArray['gam0']*pArray['charge']*gArray['dt']/pArray['mass']
    
    for ip in xrange(nPart):
        iPart  = (((pArray['x'][ip]-gArray['xMin'])                  %(gArray['xMax']-gArray['xMin']))+gArray['xMin'])/dX;
        iLeft  = int(floor(iPart));
        iRight = iBoundedRight[int(iLeft+1)];
        
        distRight = (iPart - real(iLeft));
        distLeft  = (1.0 - distRight);  
        distRight = distRight*pArray['df'][ip];
        distLeft  = distLeft*pArray['df'][ip];  
        #print(distLeft,distRight,distLeft+distRight)
        
        ep[0] = (distLeft*fArray['electricFieldX'][iLeft]) + (distRight*fArray['electricFieldX'][iRight])
        ep[1] = (distLeft*fArray['electricFieldY'][iLeft]) + (distRight*fArray['electricFieldY'][iRight])
        ep[2] = (distLeft*fArray['electricFieldZ'][iLeft]) + (distRight*fArray['electricFieldZ'][iRight])
        
        bp[0] = (distLeft*fArray['magneticFieldX'][iLeft]) + (distRight*fArray['magneticFieldX'][iRight])
        bp[1] = (distLeft*fArray['magneticFieldY'][iLeft]) + (distRight*fArray['magneticFieldY'][iRight])
        bp[2] = (distLeft*fArray['magneticFieldZ'][iLeft]) + (distRight*fArray['magneticFieldZ'][iRight])
        
        #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        #!!!         solve equation of relativiatic motion      !!!
        #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        #!!! first acceleration by electric field !!!
        um[0] = pArray['ux'][ip] + (const0*ep[0])
        um[1] = pArray['uy'][ip] + (const0*ep[1])
        um[2] = pArray['uz'][ip] + (const0*ep[2])
        
        #!!! preparing for velocity rotation !!!
        dp1 = sqrt(1.0 + dot(um[:],um[:]))
        
        bt[:] = const0*bp[:]/dp1
                
        #!!! vector product !!!
        for i in xrange(3):
            vpro[i] = (um[(i+1)%3]*bt[(i+2)%3]) - (um[(i+2)%3]*bt[(i+1)%3]) 
        for i in xrange(3):
            vproa[i] = (vpro[(i+1)%3]*bt[(i+2)%3]) - (vpro[(i+2)%3]*bt[(i+1)%3])
            
        #!!! velocity rotation !!!
        dp2 = 2.0/(1.0+dot(bt[:],bt[:]))
        
        up[:] = um[:] + (vpro[:] + vproa[:])*dp2
        
        #!!! second acceleration by electric field !!!
        pArray['ux'][ip] = up[0] + (const0*ep[0])
        pArray['uy'][ip] = up[1] + (const0*ep[1])
        pArray['uz'][ip] = up[2] + (const0*ep[2])
        
        #!!! new values !!!
        gamt = sqrt(1.0 + ((pArray['ux'][ip]*pArray['ux'][ip])                          +(pArray['uy'][ip]*pArray['uy'][ip])                          +(pArray['uz'][ip]*pArray['uz'][ip]))) 
        #print(gamt)
        pArray['vx'][ip] = pArray['ux'][ip]/gamt
        pArray['vy'][ip] = pArray['uy'][ip]/gamt 
        pArray['vz'][ip] = pArray['uz'][ip]/gamt 
        
        #print('before:',ip,nPart,pArray['x'][ip],pArray['vx'][ip],pArray['x'][ip] + pArray['vx'][ip])
        pArray['x'][ip] = pArray['x'][ip] + pArray['vx'][ip]*gArray['dt']
        pArray['x'][ip] = ((pArray['x'][ip] - gArray['xMin'])                          %(gArray['xMax'] - gArray['xMin']))                          +gArray['xMin']
        #print('after:',ip,nPart,pArray['x'][ip],pArray['vx'][ip],pArray['x'][ip] + pArray['vx'][ip])
    return
        

def getFields(fArray,dArray,gArray):
    """Gather the charge density and current density of particles in the simulation. 
    The input variables are expected to be of the type, python dict, will be modified and changed:
        dArray[*'charge or xCurrent',**'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
            *(either 'charge' or 'current')
            **(where nLengthTotal=nxLength*nyLength*nzLength)
    The output variables are expected to be of the type, python dict, will be modified and changed:
        fArray[*'electricFieldX',('electricFieldY','electricFieldZ',)
                'magneticFieldX',('magneticFieldY','magneticFieldZ',)
               **'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
            *(either 'electric' or 'magnetic')
            **(where nLengthTotal=nxLength*nyLength*nzLength)
        """
    ### Make local copies for simpler use
    nDim=fArray['nDimensions'];
    dX=fArray['dx'];
    nxLength=fArray['nxLength'];
    cc=gArray['cc'];
    
    ### Set up some arrays
    fpp=array(zeros([nxLength]));
    fmp=array(zeros([nxLength]));
    fp=array(zeros([nxLength]));
    fm=array(zeros([nxLength]));
        
    ### Set up integer boundary
    iBoundedLeft=[];
    iBoundedRight=[];
    for i in xrange(nxLength):
        iBoundedLeft.append(int(i-1));
        iBoundedRight.append(int(i+1));
    iBoundedRight[nxLength-1]=0
    iBoundedLeft[0]=nxLength-1
    
    ### transverse electric field
    et=array(zeros([nxLength]));
    et[0] = 0.0;
    for i in xrange(nxLength-1):
        et[iBoundedRight[i]] = et[i] + (cc*dArray['charge'][i]);
    #eb = mean(et);
    eb = sum(et)/(nxLength-1);
    for i in xrange(nxLength):
        fArray['electricFieldX'][i] = 0.5*(et[i] + et[iBoundedRight[i]]) - eb;
    fArray['electricFieldX'][nxLength-1] = fArray['electricFieldX'][0];
        
    ### Time integration for Ey Bz with Jy
    fpp[:] = fArray['electricFieldY'][:] + fArray['magneticFieldZ'][:];
    fmp[:] = fArray['electricFieldY'][:] - fArray['magneticFieldZ'][:];
    
    fp[:] = fpp[iBoundedLeft[:]]                     - (0.5*( dArray['yCurrent'][iBoundedLeft[:]]                            +dArray['yCurrent'][:]));
    fm[:] = fmp[iBoundedRight[:]]                     - (0.5*( dArray['yCurrent'][iBoundedRight[:]]                            +dArray['yCurrent'][:]));

    fArray['electricFieldY'][:]=0.5*(fp[:]+fm[:]);
    fArray['magneticFieldZ'][:]=0.5*(fp[:]-fm[:]);   
    
    ### Time integration for Ez By with Jz
    fpp[:] = fArray['electricFieldZ'][:] - fArray['magneticFieldY'][:];
    fmp[:] = fArray['electricFieldZ'][:] + fArray['magneticFieldY'][:];
    
    fp[:] = fpp[iBoundedLeft[:]]                     - (0.5*( dArray['zCurrent'][iBoundedLeft[:]]                            +dArray['zCurrent'][:]));
    fm[:] = fmp[iBoundedRight[:]]                     - (0.5*( dArray['zCurrent'][iBoundedRight[:]]                            +dArray['zCurrent'][:]));

    fArray['electricFieldZ'][:]=0.5*( fp[:]+fm[:]);
    fArray['magneticFieldY'][:]=0.5*(-fp[:]+fm[:]);  
    return

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!! Initialization : initGrid, initFieldDict, initDensityDict, initPartDict, 
#!!!                  initConservationDict, initDiagnosticsDict
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def initGrid(gArray,nDim,xMin,xMax,nxLength,nParticles,dt,sigma=1.0):
    """gArray['x','dx','nDim','nxLength','xMin','xMax']"""
    gArray['x']=linspace(xMin,xMax,nxLength);
    gArray['dx']=gArray['x'][1]-gArray['x'][0];
    gArray['nDim']=nDim;
    gArray['nxLength']=nxLength;
    gArray['xMin']=xMin;
    gArray['xMax']=xMax;
    gArray['dt']=dt;#gArray['x'][1]-gArray['x'][0];
    gArray['cc']=gArray['dx']/(2.0*(nParticles/nxLength)*sigma)
    return

def initFieldDict(fArray,gArray,specialForm='none',nPulse=20,ampPulse=1.0):
    """
    fArray[*'electricFieldX',('electricFieldY','electricFieldZ',)
                'magneticFieldX',('magneticFieldY','magneticFieldZ',)
               **'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
    gArray['x','dx','nDim','nxLength','xMin','xMax']
    """
    fArray['nDimensions']=gArray['nDim'];
    fArray['dx']=gArray['dx'];
    fArray['nxLength']=gArray['nxLength'];
    
    fArray['electricFieldX']=array(zeros([fArray['nxLength']]));
    fArray['magneticFieldX']=array(zeros([fArray['nxLength']]));
    
    fArray['electricFieldY']=array(zeros([fArray['nxLength']]));
    fArray['magneticFieldY']=array(zeros([fArray['nxLength']]));
    
    fArray['electricFieldZ']=array(zeros([fArray['nxLength']]));
    fArray['magneticFieldZ']=array(zeros([fArray['nxLength']]));
    
    if(specialForm=='Linear-Gaussian-Pulse'):
        xArray=linspace(0,(fArray['nxLength']-1)*fArray['dx'],(fArray['nxLength']))
        yArray=ampPulse*sin(xArray[:]*2*pi*nPulse)*exp(-0.5*abs(((xArray[:]-0.3)/(0.2/2.355))**3))
        fArray['electricFieldY'][:]=yArray[:];
        fArray['magneticFieldZ'][:]=yArray[:];
        
    return

def initDensityDict(dArray,gArray):
    """
    dArray[*'charge or xCurrent',**'nLengthTotal','nDimensions','x','dx','nxLength'
               (,'y','dy','nyLength','z','dz','nzLength')]
            *(either 'charge' or 'current')
            **(where nLengthTotal=nxLength*nyLength*nzLength)
    gArray['x','dx','nDim','nxLength','xMin','xMax']
    """
    dArray['nDimensions']=gArray['nDim'];
    dArray['dx']=gArray['dx'];
    dArray['x']=gArray['x'];
    dArray['nxLength']=gArray['nxLength'];
    
    dArray['charge']=array(zeros([dArray['nxLength']]));
    dArray['xCurrent']=array(zeros([dArray['nxLength']]));
    dArray['yCurrent']=array(zeros([dArray['nxLength']]));
    dArray['zCurrent']=array(zeros([dArray['nxLength']]));
    return

def initPartDict(pArray,gArray,nParticles,pCharge,pMass,pTemp,pDrift,specialForm='none',densityModifier=1.0):
    """
    pArray['nParticles','nDimensions','charge','mass','x','vx'
               (,'y','vy','z','vz')]
    gArray['x','dx','nDim','nxLength','xMin','xMax']
    """
    
    pArray['nDimensions']=gArray['nDim'];
    pArray['nParticles']=nParticles;
    pArray['charge']=pCharge;
    pArray['mass']=pMass;
    
    pArray['df']=(random.random(nParticles)*(gArray['xMax']-gArray['xMin']))+gArray['xMin'];
    pArray['x']=(random.random(nParticles)*(gArray['xMax']-gArray['xMin']))+gArray['xMin'];
    pArray['vx']=random.normal(pDrift[0],max(1.0e-10,pTemp[0]),nParticles);
    pArray['vy']=random.normal(pDrift[1],max(1.0e-10,pTemp[1]),nParticles);
    pArray['vz']=random.normal(pDrift[2],max(1.0e-10,pTemp[2]),nParticles);
    pArray['ux']=array(zeros([nParticles]));
    pArray['uy']=array(zeros([nParticles]));
    pArray['uz']=array(zeros([nParticles]));
    
    for ip in xrange(nParticles):
        gamt = sqrt(1.0 - ((pArray['vx'][ip]*pArray['vx'][ip])                          +(pArray['vy'][ip]*pArray['vy'][ip])                          +(pArray['vz'][ip]*pArray['vz'][ip])))        
        pArray['ux'][ip] = pArray['vx'][ip]*gamt
        pArray['uy'][ip] = pArray['vy'][ip]*gamt 
        pArray['uz'][ip] = pArray['vz'][ip]*gamt
        #print(gamt)
    pArray['gam0']=sqrt(1.0 + (mean(pArray['ux'])**2))
    #print("gam0",pArray['gam0'])
      
    if(specialForm=='full-f'):
        pArray['df'][:]=1.0;
    elif(specialForm=='Gaussian'):
        pArray['x'][:]=linspace(gArray['xMin'],gArray['xMax'],nParticles)
        pArray['df'][:]=exp(-((pArray['x'][:]-(0.5*(gArray['xMax']+gArray['xMin'])))/(0.1*(gArray['xMax']-gArray['xMin'])))**2);
    elif(specialForm=='Dirac-Delta'):
        pArray['x'][:]=linspace(gArray['xMin'],gArray['xMax'],nParticles)
        pArray['df'][:]=exp(-((pArray['x'][:]-(0.5*(gArray['xMax']+gArray['xMin'])))/(1.0e-3*(gArray['xMax']-gArray['xMin'])))**2);
    elif(specialForm=='Sine'):
        pArray['x'][:]=linspace(gArray['xMin'],gArray['xMax'],nParticles)
        pArray['df'][:]=sin(pArray['x'][:]*2*pi/(gArray['xMax']-gArray['xMin']));
    elif(specialForm=='Cosine'):
        pArray['x'][:]=linspace(gArray['xMin'],gArray['xMax'],nParticles)
        pArray['df'][:]=cos(pArray['x'][:]*2*pi/(gArray['xMax']-gArray['xMin']));
    pArray['df'][:]=pArray['df'][:]*densityModifier;
    return

def initConservationDict(cArray):
    cArray['particleEnergy']=[]
    cArray['electricFieldEnergy']=[]
    cArray['magneticFieldEnergy']=[]
    return

def initDiagnosticsDict(diagArray,
                        fVarsOfInterest=['electricFieldX','electricFieldY'],
                        dVarsOfInterest=['charge']):
    for iv in xrange(len(fVarsOfInterest)):
        diagArray[fVarsOfInterest[iv]]=[]
    for iv in xrange(len(dVarsOfInterest)):
        diagArray[dVarsOfInterest[iv]]=[]    
    return

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!! Diagnostics : getConservation, getDiagnostics, getPhaseSpace
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def getConservation(cArray,pArray,fArray,gArray):
    nParticles=pArray['nParticles'];
    nxLength=fArray['nxLength'];
    pCharge=pArray['charge'];
    pMass=pArray['mass'];
    pEnergy=0.0
    for ip in xrange(nParticles):
        pEnergy=pEnergy + ((0.5*pMass)*pArray['vx'][ip]**2)                        + ((0.5*pMass)*pArray['vy'][ip]**2)                        + ((0.5*pMass)*pArray['vx'][ip]**2)
    cArray['particleEnergy'].append(pEnergy)
    
    efEnergy=0.0
    mfEnergy=0.0
    for ifield in xrange(nxLength):
        efEnergy=efEnergy + (fArray['electricFieldX'][ifield]**2)                          + (fArray['electricFieldY'][ifield]**2)                          + (fArray['electricFieldZ'][ifield]**2)
        mfEnergy=mfEnergy + (fArray['magneticFieldX'][ifield]**2)                          + (fArray['magneticFieldY'][ifield]**2)                          + (fArray['magneticFieldZ'][ifield]**2)
    cArray['electricFieldEnergy'].append(efEnergy)
    cArray['magneticFieldEnergy'].append(mfEnergy)
    return

def getDiagnostics(diagArray,fArray,dArray,gArray,
                        fVarsOfInterest=['electricFieldX','electricFieldY'],
                        dVarsOfInterest=['charge']):
    tmp=array(zeros([fArray['nxLength']]))
    for iv in xrange(len(fVarsOfInterest)):
        tmp[:]=fArray[fVarsOfInterest[iv]][:]
        diagArray[fVarsOfInterest[iv]].append(tmp)
    for iv in xrange(len(dVarsOfInterest)):
        tmp[:]=dArray[dVarsOfInterest[iv]][:]
        diagArray[dVarsOfInterest[iv]].append(tmp)
    return

def getPhaseSpace(particlesArray,gArray,vMin=-1.0,vMax=1.0):
    nxLength=gArray['nxLength'];
    nPart=particlesArray[0]['nParticles'];
    phaseSpace=array(zeros([nxLength,nxLength]));
    vxPhaseSpace,xPhaseSpace=meshgrid(gArray['x'],linspace(vMin,vMax,nxLength));
    dX=gArray['dx'];
    dV=(vMax-vMin)/nxLength;
    
    ### Set up integer boundary
    iBoundedLeft=[];
    iBoundedRight=[];
    for i in xrange(nxLength):
        iBoundedLeft.append(int(i-1));
        iBoundedRight.append(int(i+1));
    iBoundedRight[nxLength-1]=0
    iBoundedLeft[0]=nxLength-1
    
    for iparticles in xrange(len(particlesArray)):
        pArray=particlesArray[iparticles];
        for ip in xrange(nPart):
            ixPart  = (((pArray['x'][ip]-gArray['xMin'])                      %(gArray['xMax']-gArray['xMin']))+gArray['xMin'])/dX;
            ixLeft  = int(floor(ixPart));
            ixRight = iBoundedRight[int(ixLeft+1)];

            xdistRight = (ixPart - real(ixLeft));
            xdistLeft  = (1.0 - xdistRight);  
            xdistRight = xdistRight*pArray['df'][ip];
            xdistLeft  = xdistLeft*pArray['df'][ip]; 

            ivxPart  = min((pArray['vx'][ip]-vMin)/dV,nxLength-1);
            ivxLeft  = int(floor(ivxPart));
            ivxRight = min(int(ivxLeft+1),nxLength-1);

            vxdistRight = (ivxPart - real(ivxLeft));
            vxdistLeft  = (1.0 - vxdistRight);  
            vxdistRight = vxdistRight*pArray['df'][ip];
            vxdistLeft  = vxdistLeft*pArray['df'][ip]; 

            phaseSpace[int(ixLeft),int(ivxLeft)]   = phaseSpace[int(ixLeft),int(ivxLeft)]                                                    + (xdistLeft*vxdistLeft*pArray['charge']);
            phaseSpace[int(ixRight),int(ivxLeft)]  = phaseSpace[int(ixRight),int(ivxLeft)]                                                    + (xdistRight*vxdistLeft*pArray['charge']);
            phaseSpace[int(ixLeft),int(ivxRight)]  = phaseSpace[int(ixLeft),int(ivxRight)]                                                    + (xdistLeft*vxdistRight*pArray['charge']);
            phaseSpace[int(ixRight),int(ivxRight)] = phaseSpace[int(ixRight),int(ivxRight)]                                                    + (xdistRight*vxdistRight*pArray['charge']);

    contourf(vxPhaseSpace,xPhaseSpace,zip(*phaseSpace),linspace(-abs(phaseSpace).max(),abs(phaseSpace).max(),31),alpha=0.5)
    return zip(*phaseSpace)

